# Certified™ Connect 4

## How to play
1. Click Project Overview > Releases in the sidebar.
1. Follow the instructions under the release you'd like to play.

## How to build from source
1. Install Maven if it's not installed already
1. Clone this repository to your computer
1. Navigate to the project directory in terminal (Command Prompt on Windows)
1. Run `mvn package`