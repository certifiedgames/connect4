package ga.jakeobrien.connect4;

import java.util.Scanner;

public class Main {
    private static Scanner s = new Scanner(System.in);
    private static GameBoard gameBoard;

    public static void main(String[] args) {
        System.out.println("Welcome to Certified™ Connect 4 by Jake");
        System.out.println("Please enter player 1's name:");
        String p1 = s.nextLine();
        String p1c;
        do {
            System.out.println("Please enter " + p1 + "'s game piece (1 character only)");
            p1c = s.nextLine();
        } while (p1c.length() != 1);

        System.out.println("Please enter player 2's name:");
        String p2 = s.nextLine();

        String p2c;
        do {
            System.out.println("Please enter " + p2 + "'s game piece (1 character only)");
            p2c = s.nextLine();
        } while (p2c.length() != 1);

        gameBoard = new GameBoard(p1c.charAt(0), p2c.charAt(0));

        int turn = 1;
        while (gameBoard.checkWin() == 0) {
            System.out.println(gameBoard.render());
            if (turn == 1) {
                System.out.println(p1 + "'s turn. Type a column.");
                String i = s.nextLine();
                try {
                    gameBoard.placePiece(Integer.parseInt(i), turn);
                } catch (Exception e) {
                    System.out.println("Invalid column.");
                }
                turn = 2;
            } else {
                System.out.println(p2 + "'s turn. Type a column.");
                String i = s.nextLine();
                try {
                    gameBoard.placePiece(Integer.parseInt(i), turn);
                } catch (Exception e) {
                    System.out.println("Invalid column.");
                }
                turn = 1;
            }
        }

        System.out.println(gameBoard.render());

        if (gameBoard.checkWin() == 1) {
            System.out.println(p1 + " wins!");
        } else {
            System.out.println(p2 + " wins!");
        }
    }
}
