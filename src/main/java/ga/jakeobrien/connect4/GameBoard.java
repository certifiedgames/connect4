package ga.jakeobrien.connect4;

import java.util.ArrayList;

public class GameBoard {
    private char p1char;
    private char p2char;
    private ArrayList<Column> columns = new ArrayList<>();

    public GameBoard(char p1, char p2) {
        p1char = p1;
        p2char = p2;

        for (int i = 0; i < 7; i++) {
            columns.add(new Column());
        }
    }

    public String render() {
        StringBuilder sb = new StringBuilder();
        sb.append(" 1 2 3 4 5 6 7 \n");
        for (int i = 4; i >=0; i--) {
            for (Column c : columns) {
                sb.append('|').append(String.valueOf(c.getRow(i))
                        .replaceAll("0", " ")
                        .replaceAll("1", String.valueOf(p1char))
                        .replaceAll("2", String.valueOf(p2char)));
            }
            sb.append("|\n");
        }
        return sb.toString();

    }

    public void placePiece(int col, int player) {
        if (col > 7 || col < 1) {
            throw new IllegalArgumentException("ga.jakeobrien.connect4.Column must be between 1 and 7");
        }
        columns.get(col - 1).placePiece(player);
    }

    /**
     * Checks for winners.
     *
     * @return winner, return 0 if no winner
     */
    public int checkWin() {

        //Vertical
        for (Column c : columns) {
            int streak = 0, player = 0;
            for (int i = 0; i < 4; i++) {
                if (c.getRow(i) != player) {
                    streak = 1;
                    player = c.getRow(i);
                } else {
                    streak++;
                }
                if (streak == 4 && player != 0) {
                    return player;
                }
            }
        }

        //Horizontal
        for (int i = 0; i < 5; i++) {
            int streak = 0, player = 0;
            for (Column c : columns) {
                if (c.getRow(i) != player) {
                    streak = 1;
                    player = c.getRow(i);
                } else {
                    streak++;
                }
                if (streak == 4 && player != 0) {
                    return player;
                }
            }
        }

        //Diagonal TopLeft->BottomRight
        for (int rowStart = 3; rowStart < 5; rowStart++) {
            for (int columnStart = 0; columnStart < 4; columnStart++) {
                int streak = 0, player = 0;
                for (int tile = 0; tile < 4; tile++) {
                    if (columns.get(columnStart + tile).getRow(rowStart - tile) != player) {
                        streak = 1;
                        player = columns.get(columnStart + tile).getRow(rowStart - tile);
                    } else {
                        streak++;
                    }
                    if (streak == 4 && player != 0) {
                        return player;
                    }
                }
            }
        }
        //Diagonal BottomLeft->TopRight
        for (int rowStart = 0; rowStart < 2; rowStart++) {
            for (int columnStart = 0; columnStart < 4; columnStart++) {
                int streak = 0, player = 0;
                for (int tile = 0; tile < 4; tile++) {
                    if (columns.get(columnStart + tile).getRow(rowStart + tile) != player) {
                        streak = 1;
                        player = columns.get(columnStart + tile).getRow(rowStart + tile);
                    } else {
                        streak++;
                    }
                    if (streak == 4 && player != 0) {
                        return player;
                    }
                }
            }
        }
        return 0;
    }
}
