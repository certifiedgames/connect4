package ga.jakeobrien.connect4;

public class Column {
    private int[] pieces = new int[5];

    /**
     * Adds a specific player's piece.
     *
     * @param player which player's piece to add.
     */
    public void placePiece(int player) {
        for (int i = 0; i < pieces.length; i++) {
            if (pieces[i] == 0) {
                pieces[i] = player;
                return;
            }
        }
        throw new IllegalStateException("ga.jakeobrien.connect4.Column full");
    }

    public int getRow(int r) {
        return pieces[r];
    }
}
